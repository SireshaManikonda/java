<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.dto.Employee"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>GetEmpById</title>
</head>
<body>

	<jsp:include page="HRHomePage.jsp"/>

	<center>
		<table border=2>

			<tr>
				<th>EmpId</th>
				<th>EmpName</th>
				<th>Salary</th>
				<th>Gender</th>
				<th>Email-Id</th>
			</tr>

			<tr>
				<td>  ${emp.empId}     </td>
				<td>  ${emp.empName}   </td>
				<td>  ${emp.salary}    </td>
				<td>  ${emp.gender}    </td>
				<td>  ${emp.emailId}   </td>
			</tr>

		</table>
	</center>

</body>
</html>