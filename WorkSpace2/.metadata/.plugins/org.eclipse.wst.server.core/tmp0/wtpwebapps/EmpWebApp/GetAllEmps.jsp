<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.List,com.dto.Employee"%>
    <!-- Using JSTL Core Tags -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>GetAllEmps</title>
</head>
<body>

	<jsp:include page="HRHomePage.jsp" />

	<table border='2' align="center">

		<tr>
			<th>EmpId</th>
			<th>EmpName</th>
			<th>Salary</th>
			<th>Gender</th>
			<th>Email-Id</th>
			<th colspan="2">Actions</th>
		</tr>

		<c:forEach var="emp" items="${empList}">
			<tr>
				<td>${emp.empId}</td>
				<td>${emp.empName}</td>
				<td>${emp.salary}</td>
				<td>${emp.gender}</td>
				<td>${emp.emailId}</td>
				<td><a href='EditEmployee?empId=${emp.empId}'>Edit</a></td>
				<td><a href='Delete?empId=${emp.empId}'>Delete</a></td>
			</tr>
		</c:forEach>
	</table>
	</center>
</body>
</html>
